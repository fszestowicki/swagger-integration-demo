﻿namespace CarsApi.Models
{
	public class PersonParams
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Phone { get; set; }
	}
}
