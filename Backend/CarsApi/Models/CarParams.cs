﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CarsApi.Models
{
	public class CarParams
	{
		[Required] public bool IsActive { get; set; }
		[Required] public string Brand { get; set; }
		[Required] public string Model { get; set; }

		/// <summary>
		/// Rok produkcji
		/// </summary>
		[Required] public int ProductionYear { get; set; }
		public DateTime? RegistrationDate { get; set; }

		/// <summary>
		/// Właściciel pojazdu
		/// </summary>
		public PersonParams Owner { get; set; }
	}
}
