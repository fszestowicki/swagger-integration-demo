﻿using System;
using System.Collections.Generic;

namespace CarsApi.Models
{
	public class CarDetailsDTO : CarParams
	{
		public int Id { get; set; }
		/// <summary>
		/// Lista zdarzeń na pojeździe
		/// </summary>
		public IEnumerable<CarEventDTO> Events { get; set; }
	}

	public class CarEventDTO
	{
		public DateTime Date { get; set; }
		public CarEventType Type { get; set; }
		public string Description { get; set; }
	}

	public enum CarEventType
	{
		Accident = 1,
		Service = 2,
		Other = 3
	}
}
