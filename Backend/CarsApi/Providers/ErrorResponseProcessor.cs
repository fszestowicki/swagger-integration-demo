﻿using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;

namespace CarsApi.Providers
{
	public class ErrorResponseProcessor : IOperationProcessor
	{
		public bool Process(OperationProcessorContext context)
		{
			var resolver = context.SchemaResolver;
			if (!resolver.HasSchema(typeof(ErrorResponse), false))
				resolver.AddSchema(typeof(ErrorResponse), false, context.SchemaGenerator.Generate(typeof(ErrorResponse)));
			context.OperationDescription.Operation.Responses.Add("400", new NSwag.OpenApiResponse()
			{
				Schema = new NJsonSchema.JsonSchema() { Reference = resolver.GetSchema(typeof(ErrorResponse), false) },
				Description = "Error response schema",
			});

			return true;
		}
	}

	public class ErrorResponse
	{
		public int? Code { get; set; }
		public string Message { get; set; }
	}
}
