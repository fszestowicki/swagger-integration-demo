﻿using Microsoft.AspNetCore.Http;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarsApi.Providers
{
	class OptionsMiddleware
	{
		private readonly RequestDelegate _next;

		public OptionsMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		public async Task Invoke(HttpContext context)
		{
			if (context.Request.Method == HttpMethod.Options.Method)
			{
				context.Response.StatusCode = (int)HttpStatusCode.OK;
				return;
			}

			await _next.Invoke(context);
		}
	}
}
