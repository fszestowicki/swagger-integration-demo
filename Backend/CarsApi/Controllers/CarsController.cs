﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarsApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarsApi.Controllers
{
    /// <summary>
    /// Pozwala na zarządzanie pojazdami w systemie
    /// </summary>
    [ApiController, Route("api/cars")]
    public class CarsController : ControllerBase
    {
        /// <summary>
        /// Dodaje nowy samochód
        /// </summary>
        /// <returns>Id dodanego pojazdu</returns>
        [HttpPost, Route("")]
        public int AddCar(CarParams input) => throw new NotImplementedException();

        /// <summary> Edytuje samochód </summary>
        [HttpPut, Route("{id}")]
        public void EditCar(int id, CarParams input) => throw new NotImplementedException();

        /// <summary>
        /// Pobiera listę pojazdów
        /// </summary>
        /// <param name="isActive">Stan pojazdu</param>
        /// <param name="brand">Poszukiwna marka</param>
        /// <param name="model">Poszukiwany model</param>
        /// <returns>Lista pasujących pojazdów</returns>
        [HttpGet, Route("")]
        public IEnumerable<CarDTO> GetCars(bool isActive, string brand = null, string model = null) => throw new NotImplementedException();

        /// <summary>
        /// Pobiera szczegóły pojazdu
        /// </summary>
        /// <param name="id">Id pojazdu, którego szczegóły mają zostać pobrane</param>
        /// <returns></returns>
        [HttpGet, Route("{id}")]
        public CarDetailsDTO GetCarDetails(int id) => new CarDetailsDTO()
        {
            Brand = "Volskwagen",
            Model = "Golf",
            Id = 1,
            IsActive = true,
            Events = new[]
            {
                new CarEventDTO() { Date = new DateTime(2019, 11, 4), Description = "Szkoda parkingowa", Type = CarEventType.Accident },
                new CarEventDTO() {Date = new DateTime(2020, 4, 1), Description = "Przegląd techniczny", Type = CarEventType.Service }
            },
            ProductionYear = 2018,
            Owner = new PersonParams()
            {
                FirstName = "Jan",
                LastName = "Kierujący",
                Phone = "5892494094"
            },
            RegistrationDate = new DateTime(2018, 9, 10)
        };

        /// <summary>
        /// Usuwa pojazd
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete, Route("{id}")]
        public void DeleteCar(int id) => throw new NotImplementedException();
    }
}