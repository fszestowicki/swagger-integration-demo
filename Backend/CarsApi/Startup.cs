using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarsApi.Providers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace CarsApi
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();
			services.AddOpenApiDocument(conf =>
			{
				conf.GenerateEnumMappingDescription = true;

				conf.DocumentProcessors.Add(new SecurityDefinitionAppender("Authorization", new OpenApiSecurityScheme()
				{
					Type = OpenApiSecuritySchemeType.ApiKey,
					Name = "Authorization",
					In = OpenApiSecurityApiKeyLocation.Header,
					Scheme = "bearer",
					BearerFormat = "JWT",
					Description = @"Access token should be sent in Authorization header using format 'bearer {TOKEN}'.
										E.g. 'Authorization: bearer xxxxxxxxxxxxxx'",
				}));

				conf.OperationProcessors.Add(new OperationSecurityScopeProcessor("Authorization"));
				conf.OperationProcessors.Add(new ErrorResponseProcessor());

				conf.PostProcess = d =>
				{
					d.Info.Title = "Pracownia Problemowa - Demo";
					d.Info.Contact = new OpenApiContact()
					{
						Email = "fsz@corcode.com",
						Name = "Filip Szestowicki",
					};
					d.Schemes.Add(OpenApiSchema.Https);
				};
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseHttpsRedirection();
			app.UseRouting();

			app.UseCors(builder =>
			{
				builder.AllowAnyHeader();
				builder.AllowAnyMethod();
				builder.AllowAnyOrigin();
			});
			app.UseMiddleware<OptionsMiddleware>();

			app.UseSwaggerUi3();
			app.UseOpenApi();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}
