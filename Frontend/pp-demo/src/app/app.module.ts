import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {API_BASE_URL, CarsClient} from '../api-client';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    CarsClient,
    { provide: API_BASE_URL, useValue: 'https://localhost:44366' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
