import { Component } from '@angular/core';
import {CarDetailsDTO, CarsClient} from '../api-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  car: CarDetailsDTO;

  constructor(private carsClient: CarsClient) { }

  getCarDetails(): void {
    this.carsClient.getCarDetails(1).subscribe(res => {
      this.car = res;
    }, error => console.error(error));
  }
}

